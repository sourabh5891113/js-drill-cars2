function getCarWithID(inventory, ID) {
    if(Array.isArray(inventory)){
        let car33 = inventory.find((car) => car.id === ID);
        console.log(
          `Car 33 is a ${car33.car_year} ${car33.car_make} ${car33.car_model}`
        );
    }else{
        console.log("First argument should be an array");
    }
}

module.exports = getCarWithID;
