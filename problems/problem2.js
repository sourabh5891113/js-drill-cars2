function getLastCar(inventory) {
  if (Array.isArray(inventory)) {
    let lastCar = inventory.slice(-1)[0];
    console.log(`Last car is a ${lastCar.car_make} ${lastCar.car_model}`);
  } else {
    console.log("First argument should be the inventory");
  }
}

module.exports = getLastCar;
