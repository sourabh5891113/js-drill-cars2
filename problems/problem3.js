function sortCarModels(inventory) {
    if (Array.isArray(inventory)) {
      let carModels = inventory
        .map((car) => car.car_model.toLowerCase())
        .sort((a, b) => (a < b ? -1 : 1));
      console.log(carModels);
    } else {
      console.log("First argument should be the inventory");
    }
  }
  
  module.exports = sortCarModels;