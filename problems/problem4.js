function getCarYears(inventory) {
    if (Array.isArray(inventory)) {
        let carYears = inventory.map((car)=> car.car_year);
        return carYears;
    } else {
      console.log("First argument should be the inventory");
      return [];
    }
  }
  
  module.exports = getCarYears;
  