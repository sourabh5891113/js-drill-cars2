let getCarYears = require("./problem4");
function getCarsOlderThen2000(inventory) {
  if (Array.isArray(inventory)) {
    let carYears = getCarYears(inventory);
    let olderCars = carYears.filter( carYear => carYear < 2000);
    console.log(olderCars);
    console.log(
      `there are ${olderCars.length} cars that were made before the year 2000`
    );

    return olderCars;
  } else {
    console.log("First argument should be the inventory");
  }
}

module.exports = getCarsOlderThen2000;
