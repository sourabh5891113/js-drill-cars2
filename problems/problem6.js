function getBMWandAudiCars(inventory) {
  if (Array.isArray(inventory)) {
    let BMWAndAudiArray = inventory.filter(
      (car) => car.car_make === "Audi" || car.car_make === "BMW"
    );
    console.log(JSON.stringify(BMWAndAudiArray));
  } else {
    console.log("First argument should be the inventory");
  }
}

module.exports = getBMWandAudiCars;
